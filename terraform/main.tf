provider "aws" {
  profile = "default"
  region  = "us-east-1"
  
}

resource "aws_key_pair" "example" {
  key_name   = "examplekey"
  public_key = file("C:\\Users\\Keerthi Vardhani\\.ssh\\id_rsa.pub")
}

resource "aws_security_group" "instance" {
  name = "terraform-instance"

  ingress {
    from_port   = 0
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  key_name               = aws_key_pair.example.key_name
  ami                    = "ami-07d9b9ddc6cd8dd30"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.instance.id]
  # subnet_id = "subnet-6f86e027"

  # user_data = <<-EOF
  #             #!/bin/bash
  #             echo "Hello, World" > index.html
  #             nohup busybox httpd -f -p 8080 &
  #             EOF

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:\\Users\\Keerthi Vardhani\\.ssh\\id_rsa")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline=[ 
      "sudo apt-get update",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt update -y",
      "sudo apt install -y puppetserver",
      "sudo sed -i -e 's/-Xms2g -Xmx2g / -Xms250m -Xmx250m /g' /etc/default/puppetserver",
      "sudo systemctl start puppetserver",
    ]
    
  }

  tags = {
    Name = "terraform-example"
  }
}

resource "aws_eip" "ip" {
  
  instance = aws_instance.example.id
}