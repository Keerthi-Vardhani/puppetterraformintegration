# PuppetTerraformIntegration

## Introduction

This repository contains the code and documentation for integrating Terraform and Puppet for automating the configuration management of infrastructure resources.

## Objective

The objective of this integration is to provision infrastructure using Terraform and then automatically configure the provisioned resources using Puppet. This ensures that the infrastructure is provisioned and configured consistently and according to the desired state.

## Repository Structure

- **terraform**: Contains Terraform configuration files for provisioning infrastructure named main.tf.
- **puppet**: Contains Puppet manifests and modules for configuring provisioned resources named apache.pp.

## Setup and Usage

### Prerequisites

- AWS Account
- GitLab account
- Terraform installed
- Puppet installed either on Virtual Machine or on local machine.
- ssh key is required to connect remote host via ssh.It consists of public key(id_rsa.pub) and private key(id_rsa).

### Installation Steps

1. Install VS code and terraform on local machine and add path to environmental varaibles to aviod errors.
2. Before integration puppet needed to be installed manually in local machine or virtual machine.
3. Creating AWS account and create user in IAM to authenticate or communicate with localmachine.Make sure that do aws configure to set up aws in local machine.
4. ssh key is stored in desired location that contains private and public key and make sure with the security permissions to allow to connect with remote server.

### Terraform Configuration

1. **Provider Configuration**:
   - Set up the AWS provider with the desired profile and region.

2. **Key Pair and Security Group**:
   - Define an AWS key pair to enable SSH access to the instance.
   - Create a security group allowing inbound traffic on port 8080.

3. **EC2 Instance Provisioning**:
   - Launch an EC2 instance with the specified AMI, instance type, and security group.
   - Establish an SSH connection to the instance using the provided private key.

4. **Puppet Installation and Configuration**:
   - Use remote-exec provisioner to execute commands on the instance.
   - Update package repositories and download Puppet Server.
   - Install Puppet Server and adjust JVM settings for better performance.
   - Start the Puppet Server service.

5. **Additional Configuration**:
   - Allocate an Elastic IP address for the instance.
   - Tag the resources for easier identification.

### Puppet:Apache

1. **Create Manifest File**: Create a new manifest file named `apache2.pp` in the directory `/etc/puppetlabs/code/environments/production/modules`. Ensure that the directory structure is organized as follows:

/etc/puppetlabs
└── code
└── environments
└── production
└── modules
└── apache
└── manifests
└── apache2.pp


2. **Manifest Contents**: Add the following Puppet code to the `apache2.pp` file:

```apache2.pp
package { 'apache2':
ensure => present,
}

service { 'apache2':
ensure => running,
enable => true,
}
```
### Steps 

1. Clone the repository:

    ```
    git clone <repository_url>
    ```

2. Navigate to the cloned repository directory:

    ```
    cd PuppetTerraformIntegration
    ```

3. Provision infrastructure using Terraform:

    ```
    cd terraform
    terraform init
    terraform apply
    ```

4. Here I have used Executing Puppet runs remotely on provisioned instances

### Integration Details

- Terraform configuration files are located in the "terraform" directory.
- Puppet manifests and modules are located in the "puppet" directory.
- Integration between Terraform and Puppet ensures consistent provisioning and configuration of infrastructure resources.




